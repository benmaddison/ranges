use core::cmp::Ordering;

use num_bigint::BigInt;

use super::{Domain, Iterable};

impl Domain for BigInt {
    const DISCRETE: bool = true;

    /// Always returns `self - 1`.
    fn predecessor(&self) -> Option<Self> {
        Some(self - 1_i32)
    }

    /// Always returns `self + 1`.
    fn successor(&self) -> Option<Self> {
        Some(self + 1_i32)
    }

    #[must_use]
    fn shares_neighbour_with(&self, other: &Self) -> bool {
        let other_val = other;

        let (big, small) = match self.cmp(other_val) {
            Ordering::Less => (other_val, self),
            Ordering::Equal => return false,
            Ordering::Greater => (self, other_val),
        };

        big - small == Self::from(2_i32)
    }
}

impl Iterable for BigInt {
    type Output = Self;

    fn next(&self) -> Option<Self::Output> {
        self.successor()
    }
}

#[cfg(test)]
mod tests {
    use num_bigint::BigInt;

    use crate::Domain;

    #[test]
    fn is_next_to() {
        assert!(BigInt::from(2).is_next_to(&BigInt::from(3)));
        assert!(!BigInt::from(2).is_next_to(&BigInt::from(4)));
    }

    #[test]
    fn shares_neighbour_with() {
        // self-distance
        assert!(!BigInt::from(2).shares_neighbour_with(&BigInt::from(2)));

        // "normal" value
        assert!(!BigInt::from(42).shares_neighbour_with(&BigInt::from(45)));
        assert!(!BigInt::from(45).shares_neighbour_with(&BigInt::from(42)));

        assert!(BigInt::from(42).shares_neighbour_with(&BigInt::from(44)));
        assert!(BigInt::from(44).shares_neighbour_with(&BigInt::from(42)));
    }

    #[test]
    fn predecessor() {
        assert_eq!(BigInt::from(42).predecessor(), Some(BigInt::from(41)));
    }

    #[test]
    fn successor() {
        assert_eq!(BigInt::from(42).successor(), Some(BigInt::from(43)));
    }
}
