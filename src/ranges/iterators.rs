use crate::{domain::Iterable, generic_range::GenericIterator, Domain, GenericRange, Ranges};
use alloc::vec;
use core::iter::FlatMap;

/// A type alias for the inner map function of the ranges iterator
type RangesFlatMap<T> = fn(GenericRange<T>) -> GenericIterator<T>;

impl<T> IntoIterator for Ranges<T>
where
    T: Domain + Iterable<Output = T>,
{
    type Item = T;
    type IntoIter = RangesIntoIter<T>;

    /// Creates an iterator over the elements of the current [`Ranges`]
    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        RangesIntoIter::new(self)
    }
}

/// An iterator over the elements of a [`Ranges`]
#[derive(Debug, Clone)]
pub struct RangesIntoIter<T>
where
    T: Domain + Iterable<Output = T>,
{
    /// The inner iterator
    iter: FlatMap<vec::IntoIter<GenericRange<T>>, GenericIterator<T>, RangesFlatMap<T>>,
}

impl<T> RangesIntoIter<T>
where
    T: Domain + Iterable<Output = T>,
{
    /// Creates a new range iterator from the given [`Ranges`]
    #[inline]
    pub fn new(ranges: Ranges<T>) -> Self {
        Self {
            iter: ranges.ranges.into_iter().flat_map(IntoIterator::into_iter),
        }
    }
}

impl<T> Iterator for RangesIntoIter<T>
where
    T: Domain + Iterable<Output = T>,
{
    type Item = T;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
